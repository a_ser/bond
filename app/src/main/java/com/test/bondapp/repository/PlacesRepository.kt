package com.test.bondapp.repository

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.test.bondapp.di.scope.AppScope
import com.test.bondapp.models.Record
import com.test.bondapp.models.ServerPlace
import com.test.bondapp.models.SuccessRecord
import com.test.bondapp.network.NetworkManager
import com.test.bondapp.network.handleNetworkResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.await
import javax.inject.Inject
import javax.inject.Named

@AppScope
class PlacesRepository @Inject constructor(private val networkManager: NetworkManager, @Named("hereMapApiKey") private val hereMapApiKey: String) {
    private var placesCache = emptyList<ServerPlace>()
    fun requestPlaces(location: Location, categories: List<String>, onResult: (Record<List<ServerPlace>>) -> Unit) {
        CoroutineScope(Dispatchers.IO).launch {
            val record = handleNetworkResponse {
                networkManager.placesService.getPlaces("${location.latitude},${location.longitude}", categories, hereMapApiKey).await()
            }
            if (record is SuccessRecord) {
                placesCache = record.data
            }
            CoroutineScope(Dispatchers.Main).launch {
                onResult(record)
            }
        }
    }

    fun requestPlacesById(placesIds: List<String>): LiveData<List<ServerPlace>> {
        val result = MutableLiveData<List<ServerPlace>>()
        CoroutineScope(Dispatchers.IO).launch {
            val requestedPlaces = placesCache.filter { placesIds.contains(it.id) }
            CoroutineScope(Dispatchers.Main).launch {
                result.value = requestedPlaces
            }
        }
        return result
    }
}