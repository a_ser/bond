package com.test.bondapp.repository

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.test.bondapp.di.scope.AppScope
import com.test.bondapp.models.CurrentPlace
import com.test.bondapp.models.Record
import com.test.bondapp.models.SuccessRecord
import com.test.bondapp.network.NetworkManager
import com.test.bondapp.network.handleNetworkResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.await
import javax.inject.Inject
import javax.inject.Named

@AppScope
class ReverseRepository @Inject constructor(private val networkManager: NetworkManager, @Named("hereMapApiKey") private val hereMapApiKey: String) {
    private var currentPlace: List<CurrentPlace> = emptyList()
    fun requestCurrentPlace(location: Location): LiveData<Record<List<CurrentPlace>>> {
        val result = MutableLiveData<Record<List<CurrentPlace>>>()
        CoroutineScope(Dispatchers.IO).launch {
            val resultRecord: Record<List<CurrentPlace>>
            if (currentPlace.isEmpty()) {
                val record = handleNetworkResponse {
                    networkManager.reverseService.getCurrentPosition("${location.latitude},${location.longitude}", MODE, hereMapApiKey).await()
                }
                if (record is SuccessRecord) {
                    currentPlace = record.data
                }
                resultRecord = record
            } else {
                resultRecord = Record.from(currentPlace)
            }
            CoroutineScope(Dispatchers.Main).launch {
                result.value = resultRecord
            }
        }
        return result
    }

    companion object {
        const val MODE = "retrieveAddresses"
    }
}