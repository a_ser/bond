package com.test.bondapp.repository

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.test.bondapp.di.scope.AppScope
import com.test.bondapp.models.Record
import com.test.bondapp.models.ServerCategory
import com.test.bondapp.models.SuccessRecord
import com.test.bondapp.network.NetworkManager
import com.test.bondapp.network.handleNetworkResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.await
import javax.inject.Inject
import javax.inject.Named

@AppScope
class CategoriesRepository @Inject constructor(private val networkManager: NetworkManager, @Named("hereMapApiKey") private val hereMapApiKey: String) {
    private var categoriesCache: List<ServerCategory> = emptyList()
    fun categories(location: Location): LiveData<Record<List<ServerCategory>>> {
        val result = MutableLiveData<Record<List<ServerCategory>>>()
        CoroutineScope(Dispatchers.IO).launch {
            if (categoriesCache.isEmpty()) {
                result.postValue(fetchCategories(location))
            } else {
                result.postValue(Record.from(categoriesCache))
            }
        }
        return result
    }

    fun findCategories(name: String, location: Location): LiveData<Record<List<ServerCategory>>> {
        val result = MutableLiveData<Record<List<ServerCategory>>>()
        CoroutineScope(Dispatchers.IO).launch {
            // Download categories if cache is absent
            var categories = categoriesCache
            if (categoriesCache.isEmpty()) {
                val downloadedCategories = fetchCategories(location)
                if (downloadedCategories is SuccessRecord) {
                    categories = downloadedCategories.data
                }
            }
            val searchResult = categories.filter { category -> category.title.contains(name, true) }
            result.postValue(Record.from(searchResult))
        }
        return result
    }

    fun findCategoriesById(ids: List<String>, location: Location): LiveData<List<ServerCategory>> {
        val result = MutableLiveData<List<ServerCategory>>()
        CoroutineScope(Dispatchers.IO).launch {
            var categories = categoriesCache
            if (categoriesCache.isEmpty()) {
                val downloadedCategories = fetchCategories(location)
                if (downloadedCategories is SuccessRecord) {
                    categories = downloadedCategories.data
                }
            }
            val foundCategories = categories.filter { category -> ids.contains(category.id) }
            result.postValue(foundCategories)
        }

        return result
    }

    private suspend fun fetchCategories(location: Location): Record<List<ServerCategory>> {
        val downloadedCategoriesRecord = handleNetworkResponse {
            networkManager.categoriesService.getCategories("${location.latitude},${location.longitude}", hereMapApiKey).await()
        }
        if (downloadedCategoriesRecord is SuccessRecord) {
            categoriesCache = downloadedCategoriesRecord.data
        }
        return downloadedCategoriesRecord
    }

}