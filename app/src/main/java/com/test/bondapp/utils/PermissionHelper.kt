package com.test.bondapp.utils

import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.test.bondapp.R

class PermissionHelper(private val fragment: Fragment) {
    private var onPermissionGranted: (() -> Unit)? = null
    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(fragment.context)
            .setTitle(fragment.getString(R.string.permission_denied))
            .setMessage(fragment.getString(R.string.permission_denied_message))
            .setPositiveButton(fragment.getString(R.string.app_settings)) { _, _ ->
                // send to app settings if permission is denied permanently
                val intent = Intent()
                intent.action = ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", fragment.context?.packageName, null)
                intent.data = uri
                fragment.startActivity(intent)
            }
            .setNegativeButton(fragment.getString(R.string.cancel), null)
            .show()
    }

    fun askForPermissions(permissions: List<String>, onPermissionGranted: () -> Unit) {
        val forbiddenPermissions = permissions.filter { !isPermissionsAllowed(it) }
        if (forbiddenPermissions.isNotEmpty()) {
            this.onPermissionGranted = onPermissionGranted
            if (fragment.shouldShowRequestPermissionRationale(forbiddenPermissions.first())) {
                showPermissionDeniedDialog()
            } else {
                fragment.requestPermissions(forbiddenPermissions.toTypedArray(), REQUEST_CODE)
            }
            return
        }
        onPermissionGranted.invoke()
    }

    fun isPermissionsAllowed(permission: String): Boolean {
        val context = fragment.context
        return context != null && ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray): Boolean {
        when (requestCode) {
            REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onPermissionGranted?.invoke()
                } else {
                    // permission is denied, you can ask for permission again, if you want
                    // askForPermissions()
                }
                return true
            }
        }
        return false
    }

    companion object {
        private const val REQUEST_CODE = 700
    }
}