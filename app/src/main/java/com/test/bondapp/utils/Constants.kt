package com.test.bondapp.utils

import java.util.concurrent.TimeUnit

object Constants {
    const val EMTPY_STRING: String = ""
    const val DEFAULT_ZOOM = 15f
    const val GOOGLE_MY_LOCATION_BUTTON_TAG = "GoogleMapMyLocationButton"
    val LOCATION_REQUEST_INTERVAL = TimeUnit.MINUTES.toMillis(1)
}