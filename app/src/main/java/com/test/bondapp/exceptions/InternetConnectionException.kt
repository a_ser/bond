package com.test.bondapp.exceptions

class InternetConnectionException(exception: Throwable? = null) : Exception(exception) {
}