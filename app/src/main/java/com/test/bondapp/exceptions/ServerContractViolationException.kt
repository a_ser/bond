package com.test.bondapp.exceptions

class ServerContractViolationException(msg: String) : Exception(msg) {
}