package com.test.bondapp.ui.screens.map

import android.Manifest
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.test.bondapp.R
import com.test.bondapp.extentions.disableItemAnimator
import com.test.bondapp.models.ErrorRecord
import com.test.bondapp.models.SuccessRecord
import com.test.bondapp.ui.base.BaseFragment
import com.test.bondapp.ui.screens.categories.CategoriesNavigation
import com.test.bondapp.utils.Constants
import com.test.bondapp.utils.PermissionHelper
import kotlinx.android.synthetic.main.fragment_map.*
import timber.log.Timber


class MapFragment : BaseFragment(), OnMapReadyCallback {
    private val viewModel by viewModels<MapViewModel>()
    private val permissionHelper = PermissionHelper(this)
    private val locationPermission = MutableLiveData<Boolean>()
    private var mapLiveData = MutableLiveData<GoogleMap>()
    private val markers = ArrayList<Marker>()
    private var currentPositionMarker: Marker? = null
    private lateinit var mapCategoriesSelectorView: CardView
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var placesAdapter: PlacesAdapter
    private lateinit var placesRecyclerView: RecyclerView
    private var bottomSheetBehavior: BottomSheetBehavior<ViewGroup>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view, savedInstanceState)
        observeMap()
    }

    private fun initViews(view: View, savedInstanceState: Bundle?) {
        mapFragment = childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mapCategoriesSelectorView = view.findViewById<CardView>(R.id.mapCategoriesSelectorView).apply {
            setOnClickListener { navigateToCategories() }
        }

        placesRecyclerView = view.findViewById(R.id.placesRecyclerView)
        placesRecyclerView.disableItemAnimator()
        placesAdapter = PlacesAdapter { place ->
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
            viewModel.highlightLocation(place.location)
        }
        placesRecyclerView.layoutManager = LinearLayoutManager(context)
        placesRecyclerView.adapter = placesAdapter

        bottomSheetBehavior = BottomSheetBehavior.from(placesRecyclerView)

        // Restore previous sheet state
        bottomSheetBehavior?.state =
            savedInstanceState?.getInt(BOTTOM_SHEET_STATE, BottomSheetBehavior.STATE_COLLAPSED) ?: BottomSheetBehavior.STATE_COLLAPSED
    }

    private fun observeMap() {
        mapLiveData.observe(this, Observer { map ->
            reverseMyLocationButton()
            locationPermissionChanges(map)
            observeCurrentLocation()
            observeHighlightLocation(map)
        })
    }

    /**
     * Either current or last selected position
     */
    private fun observeHighlightLocation(map: GoogleMap) {
        viewModel.highlightedLocation.observe(this, Observer { location ->
            navigateMapToPoint(map, location)
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        bottomSheetBehavior?.state?.let { outState.putInt(BOTTOM_SHEET_STATE, it)}
    }

    private fun navigateToCategories() {
        viewModel.navigateToCategories().observe(this, Observer { navigation ->
            when (navigation) {
                is CategoriesNavigation -> {
                    val extras = FragmentNavigatorExtras(mapCategoriesSelectorView to "categoriesSelector")
                    navigate(
                        MapFragmentDirections.categoriesAction(navigation.selectedCategories, navigation.location),
                        extras,
                        CATEGORIES_REQUEST_CODE
                    )
                }
            }
        })
    }

    override fun onFragmentResult(requestCode: Int, bundle: Bundle) {
        val selectedCategories = bundle.getStringArrayList(SELECTED_CATEGORIES_ID_KEY) ?: ArrayList()
        viewModel.setCategories(selectedCategories)
    }

    private fun observeCurrentLocation() {
        viewModel.location.observe(this, Observer { location ->
            mapCategoriesSelectorView.visibility = View.VISIBLE
            // do not interrupt user when location is changed
            if (viewModel.highlightedLocation.value == null) {
                viewModel.highlightLocation(location)
            }
        })

        // Add a marker with current position
        viewModel.currentPosition.observe(this, Observer { record ->
            when (record) {
                is SuccessRecord -> {
                    val firstPositionData = record.data.firstOrNull()
                    if (firstPositionData != null) {
                        currentPositionMarker?.remove()
                        mapLiveData.value?.let { map ->
                            currentPositionMarker = map.addMarker(
                                MarkerOptions()
                                    .position(LatLng(firstPositionData.location.displayPosition.lat, firstPositionData.location.displayPosition.long))
                                    .title(firstPositionData.location.address.label)
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                            )
                        }
                    }

                }

                is ErrorRecord -> {
                    Toast.makeText(context, R.string.internet_request_error, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun locationPermissionChanges(map: GoogleMap) {
        locationPermission.observe(this, Observer { isGranted ->
            updateLocationUi(map, isGranted)
            requestData()
        })
    }

    private fun navigateMapToPoint(map: GoogleMap, location: Location) {
        map.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    location.latitude,
                    location.longitude
                ), Constants.DEFAULT_ZOOM
            )
        )
    }

    /**
     * "Find me" button is located on the top
     */
    private fun reverseMyLocationButton() {
        val locationButton = mapView.findViewWithTag<View>(Constants.GOOGLE_MY_LOCATION_BUTTON_TAG)
        if (locationButton != null) {
            val rlp = locationButton.layoutParams as (RelativeLayout.LayoutParams)
            // position on right bottom
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
            val buttonMargin = context?.resources?.getDimensionPixelSize(R.dimen.mapMyLocationMargin) ?: 0
            rlp.setMargins(0, 0, buttonMargin, buttonMargin)
        }
    }

    private fun updateLocationUi(map: GoogleMap, isPermissionGranted: Boolean) {
        try {
            if (isPermissionGranted) {
                map.isMyLocationEnabled = true
                map.uiSettings?.isMyLocationButtonEnabled = true
            } else {
                map.isMyLocationEnabled = false
                map.uiSettings?.isMyLocationButtonEnabled = false
            }
        } catch (e: SecurityException) {
            Timber.e(e)
        }
    }


    override fun onStart() {
        super.onStart()
        permissionHelper.askForPermissions(listOf(Manifest.permission.ACCESS_FINE_LOCATION)) {
            locationPermission.value = true
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.setLocationUpdates()
    }

    override fun onStop() {
        super.onStop()
        viewModel.removeLocationUpdates()
    }

    private fun requestData() {
        viewModel.places.observe(this, Observer { record ->
            when (record) {
                is SuccessRecord -> {
                    markers.forEach { it.remove() }
                    markers.clear()
                    record.data.forEach { place ->
                        mapLiveData.value?.let { map ->
                            markers.add(
                                map.addMarker(
                                    MarkerOptions()
                                        .position(LatLng(place.location.latitude, place.location.longitude))
                                        .title(place.title)
                                )
                            )
                        }
                    }
                    placesAdapter.data = record.data

                    bottomSheetBehavior?.peekHeight =
                        if (record.data.isNotEmpty()) requireActivity().resources.getDimensionPixelSize(R.dimen.bottomSheetPeekHeight) else HIDDEN_BOTTOM_SHEET_HEIGHT
                }
                is ErrorRecord -> {
                    Toast.makeText(context, R.string.internet_request_error, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (!permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onMapReady(map: GoogleMap?) {
        if (map == null) return
        mapLiveData.value = map
    }

    companion object {
        const val CATEGORIES_REQUEST_CODE = 800
        const val HIDDEN_BOTTOM_SHEET_HEIGHT = 0
        const val SELECTED_CATEGORIES_ID_KEY = "selected-categories"
        const val BOTTOM_SHEET_STATE = "bottom-sheet-state"
    }
}
