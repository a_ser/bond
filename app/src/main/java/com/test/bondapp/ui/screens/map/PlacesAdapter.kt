package com.test.bondapp.ui.screens.map

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.test.bondapp.R
import com.test.bondapp.extentions.context
import com.test.bondapp.extentions.viewBind
import com.test.bondapp.models.Place

class PlacesAdapter(private val onPlaceSelected: (Place) -> Unit) :
    RecyclerView.Adapter<PlacesAdapter.PlacesViewHolder>() {
    var data: List<Place> = emptyList()
        set(value) {
            val callback = PlacesItemsCallback(value, field)
            val diffResult = DiffUtil.calculateDiff(callback)
            field = value
            diffResult.dispatchUpdatesTo(this)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlacesViewHolder =
        PlacesViewHolder(parent)

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: PlacesViewHolder, position: Int) {
        val place = data[position]
        with(holder) {
            placeItemTitleView.text = place.title
            placeItemCategoryView.visibility = if (place.categoryTitle == null) View.GONE else View.VISIBLE
            placeItemCategoryView.text = place.categoryTitle
            placeItemOpeningHoursView.visibility = if (place.openingHoursLabel == null) View.GONE else View.VISIBLE
            placeItemOpeningHoursView.text = context.getString(R.string.xs_xs, place.openingHoursLabel, place.openingHoursText)
            placeItemTagsView.visibility = if (place.tags == null) View.GONE else View.VISIBLE
            placeItemTagsView.text = context.getString(R.string.tags, place.tags?.joinToString())

            itemView.setOnClickListener {
                onPlaceSelected.invoke(place)
            }
        }
    }


    class PlacesViewHolder private constructor(view: View) : RecyclerView.ViewHolder(view) {
        val placeItemTitleView by viewBind<TextView>(R.id.placeItemTitleView)
        val placeItemCategoryView by viewBind<TextView>(R.id.placeItemCategoryView)
        val placeItemOpeningHoursView by viewBind<TextView>(R.id.placeItemOpeningHoursView)
        val placeItemTagsView by viewBind<TextView>(R.id.placeItemTagsView)

        constructor(parent: ViewGroup) : this(LayoutInflater.from(parent.context).inflate(R.layout.layout_place_item, parent, false))
    }

    class PlacesItemsCallback(private val newList: List<Place>, private val oldList: List<Place>) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return newList[newItemPosition].id == oldList[oldItemPosition].id
        }

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }

    }
}