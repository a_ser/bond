package com.test.bondapp.ui.screens.categories


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionInflater
import com.phelat.navigationresult.navigateUp
import com.test.bondapp.R
import com.test.bondapp.extentions.disableItemAnimator
import com.test.bondapp.extentions.showSoftKeyBoard
import com.test.bondapp.models.ErrorRecord
import com.test.bondapp.models.SuccessRecord
import com.test.bondapp.ui.base.BaseFragment
import com.test.bondapp.ui.screens.map.MapFragment

/**
 * A simple [Fragment] subclass.
 */
class CategoriesFragment : BaseFragment() {
    private val viewModel by viewModels<CategoriesViewModel>()
    private lateinit var categoriesAdapter: CategoriesAdapter
    private lateinit var searchCategoriesView: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_categories, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
        observeData()
        initData(savedInstanceState)
    }

    private fun initData(savedInstanceState: Bundle?) {
        // Ignore rotate state as ViewModel is already inited
        if (savedInstanceState == null) {
            arguments?.let {
                val bundle = CategoriesFragmentArgs.fromBundle(it)
                viewModel.setLocation(bundle.location)
                viewModel.setSelection(bundle.selectedCategories)
            }
        }
    }

    private fun initViews(view: View) {
        searchCategoriesView = view.findViewById(R.id.searchCategories)
        searchCategoriesView.showSoftKeyBoard()
        searchCategoriesView.doAfterTextChanged { editable ->
            editable?.let { viewModel.findCategoriesByName(it.toString()) }
        }

        val recyclerView = view.findViewById<RecyclerView>(R.id.categoriesRecyclerView)
        recyclerView.disableItemAnimator()
        recyclerView.layoutManager = LinearLayoutManager(context)
        categoriesAdapter = CategoriesAdapter { categoryId, isSelected ->
            viewModel.changeSelection(categoryId, isSelected)
        }
        recyclerView.adapter = categoriesAdapter
    }

    private fun observeData() {
        viewModel.categories.observe(this, Observer { record ->
            when (record) {
                is SuccessRecord -> {
                    categoriesAdapter.data = record.data
                }
                is ErrorRecord -> {
                    Toast.makeText(context, R.string.internet_request_error, Toast.LENGTH_LONG).show()
                }
            }
        })
        viewModel.requestData()
    }

    override fun handleOnBackPressed(): Boolean {
        viewModel.onBackPress().observe(this, Observer { selectedCategories ->
            val result = Bundle(1).apply {
                putStringArrayList(MapFragment.SELECTED_CATEGORIES_ID_KEY, selectedCategories)
            }
            navigateUp(MapFragment.CATEGORIES_REQUEST_CODE, result)
        })
        return true
    }
}
