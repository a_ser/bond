package com.test.bondapp.ui.screens.categories

import android.location.Location
import androidx.lifecycle.*
import com.test.bondapp.models.*
import com.test.bondapp.repository.CategoriesRepository
import com.test.bondapp.utils.Constants
import javax.inject.Inject

class CategoriesViewModel @Inject constructor(private val categoriesRepository: CategoriesRepository) : ViewModel() {
    private val _searchString = MutableLiveData<String>()
    private val _selectedItems = MutableLiveData<Set<String>>()
    private val _categories = MediatorLiveData<Record<List<Category>>>()
    private lateinit var location: Location
    private val serverCategories: LiveData<Record<List<ServerCategory>>> = Transformations.switchMap(_searchString) { categoryTitle ->
        if (categoryTitle.isEmpty()) {
            categoriesRepository.categories(location)
        } else {
            categoriesRepository.findCategories(categoryTitle, location)
        }
    }
    val categories: LiveData<Record<List<Category>>> = _categories

    init {
        _selectedItems.value = emptySet()
        _categories.addSource(serverCategories) {
            updateItems()
        }

        _categories.addSource(_selectedItems) {
            updateItems()
        }
    }

    private fun updateItems() {
        serverCategories.value?.let { record ->
            if (record is SuccessRecord) {
                val result = ArrayList<Category>()
                record.data.forEach { serverCategory ->
                    result.add(Category.from(serverCategory, _selectedItems.value?.contains(serverCategory.id) == true))
                }
                _categories.value = Record.from(result)
            } else if (record is ErrorRecord) {
                _categories.value = Record.from(record.throwable)
            }
        }
    }

    fun findCategoriesByName(name: String) {
        _searchString.value = name
    }

    fun changeSelection(itemId: String, isSelected: Boolean) {
        val selectedItemsSet = _selectedItems.value?.toMutableSet() ?: HashSet()
        if (isSelected) {
            selectedItemsSet.add(itemId)
        } else {
            selectedItemsSet.remove(itemId)
        }
        _selectedItems.value = selectedItemsSet
    }

    /**
     * Set previously selected categories
     */
    fun setSelection(selection: Array<String>) {
        val selectedItemsSet = HashSet<String>()
        selectedItemsSet.addAll(selection)
        _selectedItems.value = selectedItemsSet
    }

    /**
     * Set location categories is searching for
     */
    fun setLocation(location: FloatArray) {
        this.location = Location("").apply {
            latitude = location[0].toDouble()
            longitude = location[1].toDouble()
        }
    }

    /**
     * Pass data with backpress
     */
    fun onBackPress(): LiveData<ArrayList<String>> {
        return Transformations.map(_selectedItems) { items ->
            val result = ArrayList<String>()
            items.forEach { result.add(it) }
            return@map result
        }
    }

    /**
     * Request either previous search or all categories
     */
    fun requestData() {
        _searchString.value = _searchString.value ?: Constants.EMTPY_STRING
    }
}