package com.test.bondapp.ui.screens.map

import android.content.Context
import android.location.Location
import androidx.lifecycle.*
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.test.bondapp.models.*
import com.test.bondapp.repository.CategoriesRepository
import com.test.bondapp.repository.PlacesRepository
import com.test.bondapp.repository.ReverseRepository
import com.test.bondapp.ui.screens.categories.CategoriesNavigation
import com.test.bondapp.utils.Constants
import timber.log.Timber
import javax.inject.Inject


class MapViewModel @Inject constructor(
    context: Context,
    private val categoriesRepository: CategoriesRepository,
    private val placesRepository: PlacesRepository,
    private val reverseRepository: ReverseRepository
) : ViewModel() {
    private val locationService = LocationServices.getFusedLocationProviderClient(context)
    private val locationRequest: LocationRequest = LocationRequest.create()
    private val locationCallback: LocationCallback
    private val _location = MutableLiveData<Location>()
    private val _selectedCategories = MutableLiveData<List<String>>()
    private val _serverCategories = Transformations.switchMap(_selectedCategories) { selectedCategories ->
        location.value?.let { categoriesRepository.findCategoriesById(selectedCategories, it) }
    }
    private val _places = MediatorLiveData<Record<List<ServerPlace>>>()
    private val _highlightedLocation = MediatorLiveData<Location>()
    val currentPosition: LiveData<Record<List<CurrentPlace>>> = Transformations.switchMap(_location) { location ->
        reverseRepository.requestCurrentPlace(location)
    }

    val categories = _serverCategories
    val location: LiveData<Location> = _location
    val places: LiveData<Record<List<Place>>> = Transformations.map(_places) { placesRecord ->
        if (placesRecord is SuccessRecord) {
            val result = ArrayList<Place>()
            placesRecord.data.forEach { result.add(Place.from(it)) }
            return@map Record.from(result.toList())
        } else if (placesRecord is ErrorRecord) {
            return@map Record.from<List<Place>>(placesRecord.throwable)
        }
        throw IllegalStateException("Non supported $placesRecord")
    }
    val highlightedLocation: LiveData<Location> = _highlightedLocation

    init {
        locationService.lastLocation.addOnSuccessListener { location ->
            Timber.d(location.toString())
        }
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        locationRequest.interval = Constants.LOCATION_REQUEST_INTERVAL

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) return
                _location.value = locationResult.lastLocation
                Timber.d(locationResult.toString())
            }
        }

        _places.addSource(_serverCategories) { requestPlaces() }
        _places.addSource(_location) { requestPlaces() }
    }

    private fun requestPlaces() {
        val location = _location.value
        val categories = _selectedCategories.value
        if (location != null && categories != null && categories.isNotEmpty()) {
            placesRepository.requestPlaces(location, categories) {
                _places.value = it
            }
        } else {
            _places.value = Record.from(emptyList())
        }
    }

    fun setLocationUpdates() {
        locationService.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    fun removeLocationUpdates() {
        locationService.removeLocationUpdates(locationCallback)
    }

    fun navigateToCategories(): LiveData<CategoriesNavigation> {
        val result = MutableLiveData<CategoriesNavigation>()
        val selectedCategoriesId = _selectedCategories.value?.toTypedArray() ?: emptyArray()
        location.value?.let { location ->
            result.value = CategoriesNavigation(selectedCategoriesId, floatArrayOf(location.latitude.toFloat(), location.longitude.toFloat()))
        }

        return result
    }

    fun setCategories(selectedCategories: List<String>) {
        _selectedCategories.value = selectedCategories
    }

    fun highlightLocation(location: Location) {
        _highlightedLocation.value = location
    }
}