package com.test.bondapp.ui.screens.categories


sealed class Navigation

class CategoriesNavigation(val selectedCategories: Array<String>, val location: FloatArray) : Navigation()