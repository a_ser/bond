package com.test.bondapp.ui.screens.categories

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.test.bondapp.R
import com.test.bondapp.extentions.context
import com.test.bondapp.extentions.viewBind
import com.test.bondapp.models.Category

class CategoriesAdapter(private val onCategorySelectionChanged: (String, Boolean) -> Unit) :
    RecyclerView.Adapter<CategoriesAdapter.CategoryViewHolder>() {
    var data: List<Category> = emptyList()
        set(value) {
            val callback = CategoryItemsCallback(value, field)
            val diffResult = DiffUtil.calculateDiff(callback)
            field = value
            diffResult.dispatchUpdatesTo(this)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder = CategoryViewHolder(parent)

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = data[position]
        with(holder) {
            textView.text = category.title
            itemView.setOnClickListener {
                onCategorySelectionChanged.invoke(category.id, !category.isSelected)
            }
            isSelected = category.isSelected
            Glide
                .with(textView.context)
                .load(category.icon)
                .centerCrop()
                .into(holder.iconView)
        }
    }


    class CategoryViewHolder private constructor(view: View) : RecyclerView.ViewHolder(view) {
        val iconView by viewBind<ImageView>(R.id.categoryItemIconView)
        val textView by viewBind<TextView>(R.id.categoryItemTitleView)
        var isSelected = false
            set(value) {
                field = value
                itemView.setBackgroundColor(if (value) selectedColor else defaultColor)
            }
        private val defaultColor by lazy { ContextCompat.getColor(context, R.color.categoryItemDefaultBackgroundColor) }
        private val selectedColor by lazy { ContextCompat.getColor(context, R.color.categoryItemSelectedBackgroundColor) }

        constructor(parent: ViewGroup) : this(LayoutInflater.from(parent.context).inflate(R.layout.layout_category_item, parent, false))
    }

    class CategoryItemsCallback(private val newList: List<Category>, private val oldList: List<Category>) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return newList[newItemPosition].id == oldList[oldItemPosition].id
        }

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }

    }
}