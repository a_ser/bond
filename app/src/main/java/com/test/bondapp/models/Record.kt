package com.test.bondapp.models

sealed class Record<T> {
    companion object {
        fun <T> from(data: T): Record<T> = SuccessRecord(data)
        fun <T> from(throwable: Throwable?): Record<T> = ErrorRecord(throwable)
    }
}

data class SuccessRecord<T>(val data: T) : Record<T>()
data class ErrorRecord<T>(val throwable: Throwable?) : Record<T>()