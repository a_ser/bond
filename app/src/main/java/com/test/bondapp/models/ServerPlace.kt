package com.test.bondapp.models

class ServerPlace(
    val id: String,
    val position: DoubleArray,
    val title: String,
    val category: Category?,
    val tags: List<Tag>?,
    val openingHours: OpeningHours?
) {
    data class Category(val title: String)
    data class Tag(val title: String)
    data class OpeningHours(val label: String, val text: String)
}