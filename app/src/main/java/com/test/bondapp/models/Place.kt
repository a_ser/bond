package com.test.bondapp.models

import android.location.Location
import androidx.core.text.HtmlCompat

data class Place(
    val id: String,
    val title: String,
    val location: Location,
    val categoryTitle: String?,
    val tags: List<String>?,
    val openingHoursLabel: String?,
    val openingHoursText: String?
) {
    companion object {
        fun from(serverPlace: ServerPlace): Place {
            return with(serverPlace) {
                Place(
                    id, title, Location("").apply {
                        latitude = serverPlace.position[0]
                        longitude = serverPlace.position[1]
                    },
                    category?.title, tags?.map { it.title },
                    openingHours?.label, openingHours?.text?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_COMPACT).toString() }
                )
            }
        }
    }
}