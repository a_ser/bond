package com.test.bondapp.models

data class Category(val id: String, val title: String, val icon: String, val isSelected: Boolean) {
    companion object {
        fun from(serverCategory: ServerCategory, isSelected: Boolean): Category {
            return with(serverCategory) {
                Category(id, title, icon, isSelected)
            }
        }
    }
}