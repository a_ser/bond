package com.test.bondapp.models

data class ServerCategory(val id: String, val title: String, val icon: String, val type: String, val href:String, val system:String)