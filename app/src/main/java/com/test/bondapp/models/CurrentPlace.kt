package com.test.bondapp.models

import com.google.gson.annotations.SerializedName

data class CurrentPlace(@SerializedName("Location") val location: Location) {
    data class Location(@SerializedName("DisplayPosition") val displayPosition: DisplayPosition, @SerializedName("Address") val address: Address) {
        data class Address(@SerializedName("Label") val label: String)
        data class DisplayPosition(@SerializedName("Latitude") val lat: Double, @SerializedName("Longitude") val long: Double)
    }
}