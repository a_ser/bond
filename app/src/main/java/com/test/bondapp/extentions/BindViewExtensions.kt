package com.test.bondapp.extentions

import android.view.View
import androidx.annotation.IdRes
import androidx.recyclerview.widget.RecyclerView
import kotlin.LazyThreadSafetyMode.NONE

fun <T : View> RecyclerView.ViewHolder.viewBind(@IdRes idRes: Int): Lazy<T> =
    lazy(NONE) {
        itemView.findViewById<T>(idRes)
            ?: throw IllegalArgumentException("Couldn't find $idRes in the provided view")
    }
