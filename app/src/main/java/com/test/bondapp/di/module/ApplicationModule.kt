package com.test.bondapp.di.module

import dagger.Module


@Module(includes = [ActivityModule::class, NetworkModule::class])
object ApplicationModule
