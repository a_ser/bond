package com.test.bondapp.di.module

import com.test.bondapp.di.scope.FragmentScope
import com.test.bondapp.ui.screens.categories.CategoriesFragment
import com.test.bondapp.ui.screens.map.MapFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [ViewModelsModule::class])
interface FragmentsBuilderModule {

    @[ContributesAndroidInjector FragmentScope]
    fun mapFragment(): MapFragment

    @[ContributesAndroidInjector FragmentScope]
    fun categoriesFragment(): CategoriesFragment
}