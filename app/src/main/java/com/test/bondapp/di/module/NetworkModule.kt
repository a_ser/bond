package com.test.bondapp.di.module

import android.content.Context
import com.test.bondapp.BuildConfig
import com.test.bondapp.R
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Named

@Module
class NetworkModule {
    @Provides
    @Named("placesBaseURL")
    fun placesBaseUrl(context: Context) = context.getString(R.string.base_url)

    @Provides
    @Named("reverseURL")
    fun reverseBaseUrl(context: Context) = context.getString(R.string.reverse_url)

    @Provides
    @Named("hereMapApiKey")
    fun apiKey(context: Context) = context.getString(R.string.here_api_key)

    @Provides
    fun httpClient(): OkHttpClient {
        val httpClientBuilder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClientBuilder.addInterceptor(interceptor)
        }
        return httpClientBuilder.build()
    }
}