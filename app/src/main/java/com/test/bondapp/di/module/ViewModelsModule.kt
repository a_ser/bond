package com.test.bondapp.di.module

import androidx.lifecycle.ViewModel
import com.test.bondapp.ui.screens.categories.CategoriesViewModel
import com.test.bondapp.ui.screens.map.MapViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [ViewModelBuilder::class])
abstract class ViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    abstract fun bindLoginViewModel(viewmodel: MapViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CategoriesViewModel::class)
    abstract fun bindCategoriesViewModel(viewmodel: CategoriesViewModel): ViewModel
}