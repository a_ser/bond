package com.test.bondapp.di.module

import com.test.bondapp.di.scope.ActivityScope
import com.test.bondapp.ui.activities.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityModule {

    @[ContributesAndroidInjector(modules = [FragmentsBuilderModule::class]) ActivityScope]
    fun contributeMainActivity(): MainActivity
}