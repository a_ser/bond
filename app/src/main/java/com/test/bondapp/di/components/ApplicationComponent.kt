package com.test.bondapp.di.components

import android.content.Context
import com.test.bondapp.BondApp
import com.test.bondapp.di.module.ApplicationModule
import com.test.bondapp.di.scope.AppScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule


@AppScope
@Component(modules = [ApplicationModule::class, AndroidSupportInjectionModule::class])
interface ApplicationComponent : AndroidInjector<BondApp> {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): ApplicationComponent
    }
}
