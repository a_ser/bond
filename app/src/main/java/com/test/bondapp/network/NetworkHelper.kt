package com.test.bondapp.network

import com.test.bondapp.exceptions.InternetConnectionException
import com.test.bondapp.exceptions.ServerContractViolationException
import com.test.bondapp.models.ErrorRecord
import com.test.bondapp.models.Record
import com.test.bondapp.models.SuccessRecord
import timber.log.Timber

inline fun <T> handleNetworkResponse(block: () -> Response<T>): Record<T> {
    try {
        val res = block.invoke()
        if (res.error != null) {
            return ErrorRecord(ServerContractViolationException(res.error!!))
        } else {
            when (res) {
                is ResultsResponse -> {
                    return if (res.results?.items != null) {
                        SuccessRecord(res.results.items)
                    } else {
                        ErrorRecord(ServerContractViolationException("Server returns empty results"))
                    }
                }
                is ItemsResponse -> {
                    return if (res.items != null) {
                        SuccessRecord(res.items)
                    } else {
                        ErrorRecord(ServerContractViolationException("Server returns empty items"))
                    }
                }
                is RawResultsResponse -> {
                    val firstResponse = res.response.view?.firstOrNull()?.result
                    return if (firstResponse != null) {
                        SuccessRecord(firstResponse)
                    } else {
                        ErrorRecord(ServerContractViolationException("Server returns empty items"))
                    }
                }
            }
        }
    } catch (e: Exception) {
        Timber.tag("Network").w(e)
        return ErrorRecord(InternetConnectionException(e))
    }
}