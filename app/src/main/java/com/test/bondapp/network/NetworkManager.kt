package com.test.bondapp.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.test.bondapp.di.scope.AppScope
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Named

@AppScope
class NetworkManager @Inject constructor(private val httpClient: OkHttpClient, @Named("placesBaseURL") private val placesBaseUrl: String, @Named("reverseURL") private val reverseBaseUrl: String) {
    val categoriesService: CategoriesService by lazy {
        Retrofit.Builder()
            .baseUrl(placesBaseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(httpClient)
            .build()
            .create(CategoriesService::class.java)
    }

    val placesService: PlacesService by lazy {
        Retrofit.Builder()
            .baseUrl(placesBaseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(httpClient)
            .build()
            .create(PlacesService::class.java)
    }

    val reverseService: ReverseService by lazy {
        Retrofit.Builder()
            .baseUrl(reverseBaseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(httpClient)
            .build()
            .create(ReverseService::class.java)
    }
}