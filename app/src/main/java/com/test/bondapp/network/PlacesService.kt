package com.test.bondapp.network

import com.test.bondapp.models.ServerPlace
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PlacesService {
    @GET("discover/explore")
    fun getPlaces(@Query("at") coordinates: String, @Query("cat") cat: List<String>, @Query("apiKey") apiKey: String): Call<ResultsResponse<List<ServerPlace>>>
}