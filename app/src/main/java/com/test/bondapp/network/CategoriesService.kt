package com.test.bondapp.network

import com.test.bondapp.models.ServerCategory
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface CategoriesService {
    @GET("categories/places")
    fun getCategories(@Query("at") coordinates: String, @Query("apiKey") apiKey: String): Call<ItemsResponse<List<ServerCategory>>>
}