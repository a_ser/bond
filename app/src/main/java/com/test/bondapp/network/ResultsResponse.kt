package com.test.bondapp.network

import com.google.gson.annotations.SerializedName

sealed class Response<T> {
    abstract val error: String?
}

data class ResultsResponse<T>(val results: Items<T>?, override val error: String?) : Response<T>() {
    data class Items<T>(val items: T?)
}

data class ItemsResponse<T>(val items: T?, override val error: String?) : Response<T>()

data class RawResultsResponse<T>(@SerializedName("Response") val response: Response<T>, override val error: String?) : Response<T>() {
    class Response<T>(@SerializedName("View") val view: List<View<T>>?)
    class View<T>(@SerializedName("Result") val result: T?)
}
