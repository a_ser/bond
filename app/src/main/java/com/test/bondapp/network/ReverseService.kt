package com.test.bondapp.network

import com.test.bondapp.models.CurrentPlace
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ReverseService {
    @GET("reversegeocode.json")
    fun getCurrentPosition(@Query("prox") coordinates: String, @Query("mode") mode: String, @Query("apiKey") apiKey: String): Call<RawResultsResponse<List<CurrentPlace>>>
}