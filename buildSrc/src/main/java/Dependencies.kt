object App {
    const val compileSdk = 29
    const val minSdk = 21
    const val targetSdk = 29
    const val versionCode = 1
    const val versionName = "1.0.0"
}


object Versions {
    const val pluginGradle = "3.5.3"
    const val buildTools = "29.0.2"
    const val kotlin = "1.3.61"
    const val ktx = "1.1.0"

    const val constraintLayout = "1.1.3"
    const val navigation = "2.2.0"
    const val navigationResult = "1.0.0-beta01"
    const val material = "1.2.0-alpha04"
    const val lifecycle = "2.2.0-alpha02"
    const val dagger = "2.23.2"
    const val coroutines = "1.3.0"
    const val gson = "2.8.2"
    const val okhttp = "3.10.0"
    const val retrofit2 = "2.6.0"
    const val coroutinesAdapter = "0.9.2"
    const val timber = "4.7.1"
    const val playServicesMap = "17.0.0"
    const val glide = "4.11.0"
}


object Plugins {
    const val gradlePlugin = "com.android.tools.build:gradle:${Versions.pluginGradle}"
    const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val navigationSageArgs = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation}"
}


object Dependencies {
    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    const val ktx = "androidx.core:core-ktx:${Versions.ktx}"

    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val navigationUi = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
    const val navigationResult = "com.phelat:navigationresult:${Versions.navigationResult}"
    const val material = "com.google.android.material:material:${Versions.material}"
    const val lifecycleExt = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycle}"
    const val liveDataKtx = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle}"
    const val viewmodelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"

    const val dagger = "com.google.dagger:dagger:${Versions.dagger}"
    const val daggerAndroidSupport = "com.google.dagger:dagger-android-support:${Versions.dagger}"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.dagger}"
    const val daggerAnnotationProcessor = "com.google.dagger:dagger-android-processor:${Versions.dagger}"

    const val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    const val coroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"

    const val gson = "com.google.code.gson:gson:${Versions.gson}"
    const val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp}"
    const val okhttpInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}"
    const val retrofit2 = "com.squareup.retrofit2:retrofit:${Versions.retrofit2}"
    const val retrofitConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit2}"
    const val coroutinesAdapter = "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:${Versions.coroutinesAdapter}"

    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"

    const val playServicesMap = "com.google.android.gms:play-services-maps:${Versions.playServicesMap}"
    const val playServicesLocation = "com.google.android.gms:play-services-location:${Versions.playServicesMap}"

    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
}
